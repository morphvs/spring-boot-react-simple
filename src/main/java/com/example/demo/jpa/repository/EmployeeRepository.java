package com.example.demo.jpa.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.jpa.entity.Employee;

/**
 * Employee repository. create a repository using Spring Data JPA.
 * 
 * @author vladisav
 *
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	
}
