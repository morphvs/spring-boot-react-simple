package com.example.demo.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;


/**
 * This is employee bean. Note: the @Data annotation is from Project Lombok.
 * 
 * @author vladisav
 *
 */
@Data
@Entity
public class Employee {
 
	@Id 
	@GeneratedValue
    private  Long id;
    private String name;
    private int age;
    private int years;
 
    private Employee() {}
 
    public Employee(String name, int age, int years) {
        this.name = name;
        this.age = age;
        this.years = years;
    }
}
